<?php
require_once "app_config.php";
$localhost= "localhost";
$username= "root";
$password= "";

$conn= new PDO("mysql: host =$localhost;", $username, $password);
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$sql = "CREATE  DATABASE IF NOT EXISTS ".DB_NAME;
$conn->exec($sql);
    try {
        $conn= new PDO("mysql: host =$localhost;dbname=".DB_NAME, $username, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $sql = "CREATE TABLE IF NOT EXISTS users (
            id int(10) unsigned NOT NULL AUTO_INCREMENT,
            username varchar(255) NOT NULL,
            firstname varchar(255) NOT NULL,
            lastname varchar(255) NOT NULL,
            leader int(10),
            active int(1) NOT NULL,
            email varchar(255) NOT NULL,
            password varchar(255) NOT NULL,
            token char(50),
            date_token datetime,
            created_at timestamp DEFAULT CURRENT_TIMESTAMP,
            updated_at timestamp DEFAULT CURRENT_TIMESTAMP,
            PRIMARY KEY (id),
            UNIQUE KEY `user_email_unique` (email)
        ) ENGINE=InnoDB;

        INSERT INTO users (username, firstname, lastname, email, password,active) VALUES (
            'root',
            'ROOT',
            'ROOT',
            'root@localhost',
            'SHWj9UVUhIjDU',
            1);
         CREATE TABLE IF NOT EXISTS  roles (
        id int(10) unsigned NOT NULL AUTO_INCREMENT,
        name varchar(50) NOT NULL,
        PRIMARY KEY (id),
        UNIQUE KEY `role_unique` (name)
        ) ENGINE=InnoDB;

           INSERT INTO roles (name) VALUES ('Administrator'), ('Moderator');


        CREATE TABLE IF NOT EXISTS  memberships (
            id int(10) unsigned NOT NULL AUTO_INCREMENT,
            user_id int(10) unsigned NOT NULL,
            role_id int(10) unsigned NOT NULL,
            PRIMARY KEY (id),
            FOREIGN KEY (user_id)
            REFERENCES users(id)
            ON DELETE CASCADE,
            FOREIGN KEY (role_id)
            REFERENCES roles(id)
            ON DELETE CASCADE,
            UNIQUE KEY `membership_unique` (user_id, role_id)
            ) ENGINE=InnoDB;
        INSERT INTO memberships (user_id, role_id) VALUES (1, 1), (1, 2);
        CREATE TABLE IF NOT EXISTS  status(
             id int(10) unsigned NOT NULL AUTO_INCREMENT,
              status_content text NOT NULL,
              users  varchar(255) NOT NULL,
              times time NOT NULL,
              dates date NOT NULL,
              PRIMARY KEY (id)

        ) ENGINE=InnoDB;
        CREATE TABLE IF NOT EXISTS  resigned(
         id int(10) unsigned NOT NULL AUTO_INCREMENT,
              content text NOT NULL,
              starttime date NOT NULL,
              lasttime date NOT NULL,
              emailto varchar(255) NOT NULL,
              emailform varchar(255) NOT NULL,
              time datetime NOT NULL,
              status varchar(10) NOT NULL,
              startdate  varchar(10) NOT NULL,
              lastdate  varchar(10) NOT NULL,
              PRIMARY KEY (id)

         ) ENGINE=InnoDB; ";
        $conn->exec($sql);
            echo " Created  Table successfully";
    } catch (PDOException $e) {
         echo $e->getMessage();
    }

$conn = null;
?>