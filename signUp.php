<?php
session_start();
/**
 * Created by PhpStorm.
 * User: ThoMas
 * Date: 28/06/2017
 * Time: 10:52 AM
 */
function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
?>
<html>
<head>
    <title>SIGN UP</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <link href="font_awesome/css/font-awesome.min.css" rel="stylesheet"/>
</head>
<body bgcolor = "#FFFFFF">
<div class="container" id="container">
    <?php include "_nav.php";?>
</div>

<div class="area" style=" float: right;">
    <div style="margin-left: 5%">
    <h2>Register</h2>
        <form role="form" class="form-horizontal" method="post" action="signUp.php">
            <div class="form-group" >
                <label for="username"  class="control-label col-sm-2">Username:</label>
                <div class="col-sm-4">
                    <input type="text" name="username" id="username" class="form-control">
                </div>
            </div>

            <div class="form-group">
                <label for="password" class="control-label col-sm-2">Password:</label>
                <div class="col-sm-4">
                    <input type="password" name="password" class="form-control" >
                    <p class="help-block text-warning" style="color: red"></p>
                </div>
            </div>

            <div class="form-group">
                <label for="confirm_password" class="control-label col-sm-2">Confirm password: </label>
                <div class="col-sm-4">
                    <input type="password" name="confirm_password" class="form-control">
                    <p class="help-block text-warning" style="color: red"></p>
                </div>
            </div>

            <div class="form-group">
                <label for="Firstname" class="control-label col-sm-2">Firs tname:</label>
                <div class="col-sm-4">
                    <input type="text" name="firstname" class="form-control">
                </div>
            </div>

            <div class="form-group">
                <label for="Lastname" class="control-label col-sm-2">Last name:</label>
                <div class="col-sm-4">
                    <input type="text" name="lastname" class="form-control">
                </div>
            </div>

            <div class="form-group">
                <label for="email" class="control-label col-sm-2">Email:</label>
                <div class="col-sm-4">
                    <input type="email" name="email" class="form-control">
                </div>
            </div>

            <div style="margin-left: 17%">
                <button class="btn btn-default" type="submit" name="submit">Create</button>
            </div>

            <div style="margin-left: 32%;">
                <p style="display: inline-block; margin-right: 10px;"><a href="getemail.php">Forgot password?</a></p>
                <p style="display: inline-block;"><a href="login.php">Sign in</a></p>
            </div>
        </form>
    </div>
</div>

</body>
</html>

<?php
    
    $conn = new PDO("mysql: host=localhost;dbname=picture", "root", "");
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
    $validated = true;
    $active= "1";
    
    if (isset($_POST['submit'])) {
        $username = $_POST['username'];
        $password= $_POST['password'];
        $firstname= $_POST['firstname'];
        $lastname= $_POST['lastname'];
        $email= $_POST['email'];
        
        // start: kiem tra form nhap vao co thoa man
        if (empty($_POST['username'])) {
		$usernameErr = "Username is required";
		$validated = false;
    	} else {
    		$username = test_input($_POST['username']);
    		if (!preg_match("/^[a-zA-Z0-9._]*$/", $username)) {
    			$usernameErr = "Username can contain only letters, numeric characters, period and underscore";
    			$validated = false;
    		}            
    	}
        
        if (empty($_POST['password'])) {
		$passwordErr = "Password is required";
		$validated = false;
    	} elseif (empty($_POST['confirm_password']) || ($_POST['confirm_password'] != $_POST['password']) ) {
    		$validated = false;
    		$passwordErr = "Please re-enter your password";
    	} elseif (strlen($_POST['password']) < 3) {
    		$passwordErr = "Password must be at least 3 characters";
    		$validated = false;
    	} else {
    		$password = test_input($_POST['password']);
    	}
        
        if (empty($_POST['firstname'])) {
		$firstnameErr = "First name is required";
		$validated = false;
    	} else {
    		$firstname = test_input($_POST['firstname']);
    		if (!preg_match("/^[a-zA-Z ]*$/", $firstname)) {
    			$firstnameErr = "First name can contain only letters and white space";
    			$validated = false;
    		}
    	}
    
    	if (empty($_POST['lastname'])) {
    		$lastnameErr = "Last name is required";
    		$validated = false;
    	} else {
    		$lastname = test_input($_POST['lastname']);
    		if (!preg_match("/^[a-zA-Z ]*$/", $lastname)) {
    			$lastnameErr = "Last name can contain only letters and white space";
    			$validated = false;
    		}
    	}
        
        if (empty($_POST['email'])) {
		$validated = false;
		$emailErr = "Email is required";
    	} else {
    		$email = test_input($_POST['email']);
    		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    			$validated = false;
    			$emailErr = "Invalid email";
    		}
    	}
        // End: form nhap vao co dung dan
        
        if ($validated) { 
		try {
		  
          //: kiem tra da ton ton username va email chua
		  $sql = "SELECT * FROM users WHERE username = ? OR email = ?";
          $result = $conn->prepare($sql);
          $result->execute(array($username, $email));//:end
          
          if ($result->rowCount()>0){//:neu da ton tai
            echo "khong the tao tai khoan moi";
          }else{//: khong ton tai
            $insert = "insert into users (active,email,firstname,lastname,password,username) value(1,'$email','$firstname','$lastname',sha1('$password'),'$username')";
            $conn->prepare($insert);
            $conn->exec($insert);
            
            //: kiem tra thong tin vua insert da thanh cong hay chua
            $sql = "SELECT * FROM users WHERE username = ? OR email = ?";
            $result = $conn->prepare($sql);
            $result->execute(array($username, $email));
            if ($result->rowCount()>0){
                echo "tao tai khoan thanh cong";
            }
          }
          
		} catch (PDOException $e) {
			echo $e->getMessage();
		}
	   }
    }
    
?>