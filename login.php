<?php
session_start();
/**
 * Created by PhpStorm.
 * User: ThoMas
 * Date: 28/06/2017
 * Time: 10:52 AM
 */
 function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
?>
<?php
if(isset($_SESSION['login'])){
    header("location: home.php");
}elseif(isset($_POST["submit"])){
    $errors = array();
    $required = array('username', 'password');
    foreach($required as $fieldname){
        if(!isset($_POST[$fieldname]) || empty($_POST[$fieldname])){
            $errors[] = "the <strong> {$fieldname} </strong> was left blank";
        }
    } //End: foreeach

    if(empty($errors)){
        $conn = mysqli_connect('localhost','root','','picture') or die ('Khong the ket noi database');
//        if (!$conn) {
//            die("Connection failed: " . mysqli_connect_error());
//        }
//        echo "Connected successfully";
        $username = mysqli_real_escape_string($conn, $_POST['username']);
        $password = mysqli_real_escape_string($conn, $_POST['password']);
        $hash_pw = sha1($password);
        //setcookie("name", "Tran Minh Chinh", time()+3600, "/","", 0);
        //setcookie("age", "25", time()+3600, "/", "",  0);

        $query = "SELECT *
                  FROM users
                  WHERE username='{$username}' AND password='{$hash_pw}'
                  LIMIT 1";
        $result = mysqli_query($conn,$query) or die(mysqli_error($conn));
        if(mysqli_num_rows($result) == 1){
            $_SESSION["login"]=$username;
            header('Location: home.php');
//                echo  "Da ket noi database";
//                redirect("welcome.php");
        } else{
            $errors[] = "Username or Password do not match those on file.";
        }
    }
}
?>
<?php
if(!empty($errors)){
    echo "<ul>";
    foreach ($errors as $error){
        echo "<li>$error</li>";
    }
    echo "</ul>";
}
?>
<?php
//    $validated = true;
//    if (isset($_SESSION['login']) && !empty($_SESSION['login'])) {
//        header("location: home.php");
//    }elseif(isset($_POST['submit']) && empty($_SESSION['login'])){
//        if (empty($_POST['username'])) {
//    		$err = "Invalid username or password";
//    		$validated = false;
//    	} else {
//    		$username = test_input($_POST['username']);
//    	}
//
//    	if (empty($_POST['password'])) {
//    		$err = "Invalid username or password";
//    		$validated = false;
//    	} else {
//    		$password = test_input($_POST['password']);
//    	}
//
//   	}
//?>
<html>
<head>
    <title>Login Page</title>
    <style type = "text/css">
        body {
            font-family:Arial, Helvetica, sans-serif;
            font-size:14px;
        }

        label {
            font-weight:bold;
            width:100px;
            font-size:14px;
        }

        .box {
            border: indianred solid 1px;
        }
    </style>

</head>
<body bgcolor = "#FFFFFF">
<div align = "center">
    <div style = "width:300px; border: solid 1px #333333; " align = "left">
        <div style = "background-color:#333333; color:#FFFFFF; padding:3px;"><b>Login</b></div>
        <div style = "margin:30px">
            <form action method = "post">
                <label>User Name  :<br/></label><input type = "User Name" name = "username" class = "box"/><br /><br />
                <label>Password  :</label><input type = "password" name = "password" class = "box" /><br/><br />
                <input type = "submit" name="submit"/><br />
            </form>
            <p><a href="signup.php" style="text-decoration: none">Sign up.</a></p>
        </div>
    </div>
</div>
</body>
</html>