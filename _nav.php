<?php
$username = $_SESSION["login"];
/**
 * Created by PhpStorm.
 * User: ThoMas
 * Date: 27/06/2017
 * Time: 3:11 PM
 */
?>
<?php
if (!isset($_SESSION['login']))
    die(header("location: login.php"));
?>
<html>
<head>
    <style>
    </style>
    <link rel="stylesheet" href="./include/default.css">
    <link rel="stylesheet" href="./include/customize.css">
    <link href="font_awesome/css/font-awesome.min.css" rel="stylesheet"/>
</head>
<body>
    <nav class="main-menu" style="position: fixed;">
    <ul>
        <li>
            <a href="home.php">
                <i class="fa fa-home fa-2x"></i>
                <span class="nav-text">
                            Dashboard
                        </span>
            </a>

        </li>
        <li class="has-subnav">
            <a href="profile.php">
                <i class="fa fa-laptop fa-2x"></i>
                <span class="nav-text">
                            PROFILE
                        </span>
            </a>

        </li>
        <li class="has-subnav">
            <a href="signUp.php">
                <i class="fa fa-list fa-2x"></i>
                <span class="nav-text">
                            CREATE ACCOUNT
                        </span>
            </a>

        </li>
        <li class="has-subnav">
            <a href="changepassword.php">
                <i class="fa fa-folder-open fa-2x"></i>
                <span class="nav-text">
                            Change PassWord
                        </span>
            </a>

        </li>
        <li>
            <a href="#">
                <i class="fa fa-bar-chart-o fa-2x"></i>
                <span class="nav-text">
                            Graphs and Statistics
                        </span>
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-font fa-2x"></i>
                <span class="nav-text">
                            <?php echo "$username"; ?>
                        </span>
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-table fa-2x"></i>
                <span class="nav-text">
                            Calendar
                        </span>
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-map-marker fa-2x"></i>
                <span class="nav-text">
                            Maps
                        </span>
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-info fa-2x"></i>
                <span class="nav-text">
                            Documentation
                        </span>
            </a>
        </li>
    </ul>

    <ul class="logout">
        <li>
            <a href="logout.php">
                <i class="fa fa-power-off fa-2x"></i>
                <span class="nav-text">
                            Logout
                </span>
            </a>
        </li>
    </ul>
</nav>
</body>
</html>