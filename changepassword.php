<?php
session_start();
/**
 * Created by PhpStorm.
 * User: ThoMas
 * Date: 2/07/2017
 * Time: 7:46 AM
 */
function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
?>
<?php
$validated = true;
$username = $_SESSION["login"];
if (isset($_POST['submit'])) {
    if (empty($_POST['newpwd'])) {
        $validated = false;
        $newpwdErr = "New password is required";
    } elseif ( empty($_POST['confirmpwd']) || ($_POST['confirmpwd'] != $_POST['newpwd']) ) {
        $validated = false;
        $newpwdErr = "Please re-confirm your password";
    } else {
        $newpwd = test_input($_POST['newpwd']);
        if (strlen($newpwd) < 3) {
            $validated = false;
            $newpwdErr = "Password must be at least 3 character";
        }
    }
    if (empty($_POST['oldpwd'])) {
        $validated = false;
        $oldpwdErr = "Please enter your old password";
    } else {
        $oldpwd = test_input($_POST['oldpwd']);
    }
    if ($validated) {
        try {
            $conn = new PDO("mysql: host=localhost;dbname=picture", "root", "");
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $update_user = "UPDATE users SET password=:newpwd WHERE username=:username AND password=:oldpwd";
            $stmt = $conn->prepare($update_user);
            $newpwd_hash = sha1($newpwd);
            $oldpwd_hash = sha1($oldpwd);
            $stmt->bindParam(':newpwd', $newpwd_hash);
            $stmt->bindParam(':oldpwd', $oldpwd_hash);
            $stmt->bindParam(':username', $username);
            $stmt->execute();
            if ($stmt->rowCount() == 1) {
                $success = true;
            } else {
                $validated = false;
                $oldpwdErr = "Incorrect password";
            }
            unset($stmt);
            $conn = null;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
}
?>
?>

<html>
<head>
    <title>Change Password</title>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="./include/default.css">
    <link rel="stylesheet" href="./include/customize.css">
</head>
<body>
    <div class="container" id="container">
        <?php include "_nav.php";?>
    </div>

    <div class="area" style=" float: right;">
        <div style="margin-left: 5%">
            <h2>Change Password</h2>

            <form class="form-horizontal" role="form" method="post" action="changepassword.php">
                <div class="form-group">
                    <label class="control-label col-sm-2" for="newpwd">Old password:</label>
                    <div class="col-sm-6">
                        <input type="password" class="form-control" name="oldpwd" id="oldpwd">
                        <p class="help-block text-warning"><?php //echo $oldpwdErr;?></p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="newpwd">New password:</label>
                    <div class="col-sm-6">
                        <input type="password" class="form-control" name="newpwd" id="newpwd">
                        <p class="help-block text-warning"><?php //echo $newpwdErr;?></p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="newpwd">Confirm new password:</label>
                    <div class="col-sm-6">
                        <input type="password" class="form-control" name="confirmpwd" id="confirmpwd">
                        <p></p>
                    </div>
                </div>

                <div style="margin-left: 690px">
                    <button class="btn btn-default" type="submit" name="submit">Save</button>
                </div>
            </form>
        </div>
    </div>

</body>
</html>