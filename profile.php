<?php
session_start();
/**
 * Created by PhpStorm.
 * User: ThoMas
 * Date: 2/07/2017
 * Time: 7:46 AM
 */
function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
?>

<html>
<head>
    <title>Edit Profile</title>
    <link rel="stylesheet" href="./include/default.css">
    <link rel="stylesheet" href="./include/customize.css">
</head>
<?php
//require_once dirname(__DIR__).'/app_config.php';
//require_once dirname(__DIR__).'/lib/util.php';
//session_start();
$date_current= date('Y-m-d');
$head_month= date('Y-m-01', strtotime($date_current));
$username = "datyd";
$firstnameErr = $lastnameErr = $emailErr = $errtime="";

try {
    $conn = new PDO("mysql: host=localhost;dbname=picture", "root", "");
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $select_user = "SELECT * FROM users WHERE username='$username'";
    $stmt = $conn->query($select_user);
    $stmt->setFetchMode(PDO::FETCH_ASSOC);
    $users = $stmt->fetchAll();
    unset($stmt);
    $conn = null;
} catch (PDOException $e) {
    echo $e->getMessage();
}//:Tim kiem user trong database


if (!(isset($users) && count($users) == 1)) {
    die("There's something wrong with the data! User duplicated or not found!");
} else {
    $firstname = $users[0]['firstname'];
    $lastname = $users[0]['lastname'];
    $email = $users[0]['email'];
}//: Lay cac thong tin ca nhan cua username. Ket qua truy van user la duy nhat thi moi lay thong tn

$validated = true;
if (isset($_POST['submit'])) {
    if (empty($_POST['firstname'])) {
        $firstnameErr = "First name is required";
        $validated = false;
    } else {
        $firstname = test_input($_POST['firstname']);
        if (!preg_match("/^[a-zA-Z ]*$/", $firstname)) {
            $firstnameErr = "First name can contain only letters and white space";
            $validated = false;
        }
    }
    if (empty($_POST['lastname'])) {
        $lastnameErr = "Last name is required";
        $validated = false;
    } else {
        $lastname = test_input($_POST['lastname']);
        if (!preg_match("/^[a-zA-Z ]*$/", $lastname)) {
            $lastnameErr = "Last name can contain only letters and white space";
            $validated = false;
        }
    }
    if (empty($_POST['email'])) {
        $validated = false;
        $emailErr = "Email is required";
    } else {
        $email = test_input($_POST['email']);
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $validated = false;
            $emailErr = "Invalid email";
        }
    }
    if ($validated) {
        try {
            $conn = new PDO("mysql: host=localhost;dbname=picture", "root", "");
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $select_user = "SELECT username, email FROM users WHERE email='$email'";
            $users = $conn->query($select_user);
            $users->setFetchMode(PDO::FETCH_ASSOC);
            foreach ($users->fetchAll() as $val) {
                if (($email == $val['email']) && ($username != $val['username'])) {
                    $validated = false;
                    $emailErr = "Email already exists";
                }
            }
            unset($val);
            if ($validated) {
                $update_user = "UPDATE users SET firstname=:firstname,lastname=:lastname,email=:email WHERE username='$username'";
                $stmt = $conn->prepare($update_user);
                $stmt->bindParam(':firstname', $firstname);
                $stmt->bindParam(':lastname', $lastname);
                $stmt->bindParam(':email', $email);
                $stmt->execute();
                unset($stmt);
                $conn = null;
                $success = true;
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
}
?>
<body>
<div class="container" id="container">
    <?php include "_nav.php";?>
</div>

<div class="area" style=" float: right;">
    <div style="margin-left: 5%;">
        <?php if (isset($success) && $success):?>
            <p class="text-success bg-success">You've successfully updated your profile!</p>
        <?php endif;?>
        <h2>My Profile</h2>
        <form role="form" class="form-horizontal" method="post" action="profile.php">
            <div class="form-group">
                <label class="control-label col-sm-2" for="username">Username:</label>
                <div class="col-sm-6">
                    <input readonly class="form-control" type="text" name="username" id="username" value="<?php echo $username; ?>">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-2" for="firstname">First name:</label>
                <div class="col-sm-6">
                    <input class="form-control" type="text" name="firstname" id="firstname" value="<?php echo $firstname; ?>">
                    <p class="help-block text-warning"><?php echo $firstnameErr; ?></p>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-2" for="lastname">Last name:</label>
                <div class="col-sm-6">
                    <input class="form-control" type="text" name="lastname" id="lastname" value="<?php echo $lastname; ?>">
                    <p class="help-block text-warning"><?php echo $lastnameErr; ?></p>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-2" for="email">Email:</label>
                <div class="col-sm-6">
                    <input class="form-control" type="text" name="email" id="email" value="<?php echo $email; ?>">
                    <p class="help-block text-warning"><?php echo $emailErr; ?></p>
                </div>
            </div>

            <button class="btn btn-default" type="submit" name="submit">Update</button>
        </form>
    </div>
</div>
</body>
</html>
